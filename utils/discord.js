export default {
  normalAvatarURL(user) {
    return `
    https://cdn.discordapp.com/avatars/${user.id}/${user.avatar}.${
      user.avatar.startsWith('a_') ? 'gif' : 'png'
    }
    `
  },
  defaultAvatarURL(user) {
    return `
    https://cdn.discordapp.com/embed/avatars/${user.discriminator % 5 ||
      '1'}.png
    `
  }
}
