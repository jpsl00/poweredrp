export default {
  getHash(text) {
    return text
      .toLowerCase()
      .normalize('NFD')
      .replace(/ /g, '-')
      .replace(/[\u0300-\u036f]/g, '')
  },

  getName(id) {
    if (!id) return
    else return ['Gerais', 'Profissão'][id - 1]
  },

  getHero(path) {
    return {
      gerais: {
        title: 'Regras Gerais',
        subtitle: ''
      },
      jobs: {
        title: 'Regras de Profissão',
        subtitle: ''
      }
    }[path]
  },

  getPath(id) {
    if (!id) return
    else return ['gerais', 'jobs'][id - 1]
  }
}
