export default {
  roleCheck(auth, minimum) {
    if (!auth || !auth.user) {
      return false
    } else if (auth.user.role >= minimum) return true
  }
}
