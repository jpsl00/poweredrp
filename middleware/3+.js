export default function({ app }) {
  if (!app.$auth || !app.$auth.user || !app.$auth.user.role) {
    return app.$auth.redirect('home')
  } else if (app.$auth.user.role < 3) return app.$auth.redirect('home')
}
