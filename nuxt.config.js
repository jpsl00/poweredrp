require('dotenv').config()

module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: 'PoweredRP',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: "Servidor de DarkRP no Garry's Mod, venha jogar!"
      },
      // Twitter summary
      {
        name: 'twitter:card',
        content: 'summary'
      },
      {
        name: 'twitter:title',
        content: 'PoweredRP'
      },
      {
        name: 'twitter:description',
        content: "Servidor de DarkRP no Garry's Mod, venha jogar!"
      },
      {
        name: 'twitter:image',
        content: 'https://poweredrp.herokuapp.com/favicon.png'
      }
    ],
    script: [
      {
        src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'
      }
      /* {
        src: '/js/burgerMenu.js'
      } */
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/png',
        href: '/favicon.png'
      },
      {
        rel: 'stylesheet',
        href: '/css/transitions.css'
      },
      {
        rel: 'stylesheet',
        href: '/css/global.css'
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Righteous'
      }
    ],
    bodyAttrs: {
      class: 'has-navbar-fixed-top'
    }
  },

  /*
  ** Customize the progress-bar color
  */
  loading: {
    color: '#209cee',
    continuous: true
  },

  /* loadingIndicator: {
    name: 'chasing-dots',
    color: '#3B8070',
    background: 'white'
  }, */

  /*
  ** Global CSS
  */
  css: [
    {
      src: 'bulma-extensions/dist/css/bulma-extensions.min.css',
      lang: 'css'
    },
    {
      src: '@fortawesome/fontawesome-free/scss/fontawesome.scss',
      lang: 'scss'
    }
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '~/plugins/vue-scrollto', ssr: false },
    { src: '~/plugins/aos', ssr: false }
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    // Doc: https://github.com/nuxt-community/modules/tree/master/packages/bulma
    '@nuxtjs/bulma',
    // Doc: https://github.com/vaso2/nuxt-fontawesome
    /* 'nuxt-fontawesome', */
    // Doc: https://github.com/nuxt-community/modules/tree/master/packages/markdownit
    '@nuxtjs/markdownit',
    // Doc: https://github.com/nuxt-community/google-gtag
    '@nuxtjs/google-gtag',
    // Doc: https://github.com/nuxt-community/auth-module
    '@nuxtjs/auth',
    // Doc: https://github.com/nuxt-community/proxy-module
    '@nuxtjs/proxy',
    // Doc:https://github.com/nuxt-community/google-adsense-module
    '@nuxtjs/google-adsense',
    // Doc: https://github.com/buefy/nuxt-buefy
    'nuxt-buefy',
    // Doc: https://github.com/nuxt-community/style-resources-module
    '@nuxtjs/style-resources'
  ],

  /**
   * Style Resource configuration
   */
  styleResources: {
    scss: ['~assets/main.scss']
  },

  /**
   * Buefy configuration
   */
  buefy: {
    /* materialDesignIcons: false, */
    materialDesignIconsHRef:
      'https://use.fontawesome.com/releases/v5.8.1/css/all.css',
    css: false,
    defaultIconPack: 'fas',
    defaultDayNames: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
    defaultMonthNames: [
      'Janeiro',
      'Fevereiro',
      'Março',
      'Abril',
      'Maio',
      'Junho',
      'Julho',
      'Agosto',
      'Setembro',
      'Outubro',
      'Novembro',
      'Dezembro'
    ]
  },

  /**
   * OAuth2 configuration
   */
  auth: {
    strategies: {
      local: {
        _scheme: 'oauth2',
        authorization_endpoint: 'https://discordapp.com/api/oauth2/authorize',
        userinfo_endpoint: '/api/auth/user',
        /* access_token_endpoint: '/api/auth/login', */
        scope: ['identify', 'connections', 'guilds.join'],
        response_type: 'token',
        token_type: 'Bearer',
        client_id: process.env.CLIENT_ID
        /* grant_type: 'authorization_code' */
      }
    },
    redirect: {
      login: '/oauth2/login',
      logout: '/oauth2/login',
      callback: '/oauth2/callback',
      home: '/'
    },
    cookie: {
      options: {
        secure: process.env.NODE_ENV === 'production'
      }
    },
    resetOnError: true,
    watchLoggedIn: true,
    rewriteRedirects: true
  },

  /**
   * Google-analytics configuration
   */
  'google-gtag': {
    id: process.env.GA_ID,
    config: {
      send_page_view: true
    },
    debug: false
  },

  /**
   * Google-adsense configuration
   */
  'google-adsense': {
    id: process.env.AS_ID,
    pageLevelAds: true,
    tag: 'ad',
    test: !(process.env.NODE_ENV === 'production')
  },

  /**
   * Markdown-it configuration
   */
  markdownit: {
    preset: 'default',
    injected: true
  },

  /**
   * FontAwesome configuration
   */
  fontawesome: {
    component: 'fa',
    imports: [
      {
        set: '@fortawesome/free-solid-svg-icons',
        icons: ['fas']
      },
      {
        set: '@fortawesome/free-regular-svg-icons',
        icons: ['far']
      },
      {
        set: '@fortawesome/free-brands-svg-icons',
        icons: ['fab']
      }
    ]
  },

  /*
  ** Axios module configuration
  */
  axios: {
    proxy: true,
    debug: false

    // See https://github.com/nuxt-community/axios-module#options
  },

  /**
   * Proxy module configuration
   */
  proxy: {
    '/api': {
      target: process.env.BASE_URL,
      changeOrigin: true,
      pathRewrite: {
        '^/api/rules': 'rules/incoming_webhook',
        '^/api/auth': 'auth/incoming_webhook',
        '^/api/staff': 'staff/incoming_webhook'
      }
    }
  },

  /*
  ** Build configuration
  */
  build: {
    postcss: {
      preset: {
        features: {
          customProperties: true
        }
      }
    },

    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
